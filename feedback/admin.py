from django.contrib import admin
from feedback.models import feed

class feedAdmin(admin.ModelAdmin):
    list_display = ['id','text','on_date']

admin.site.register(feed,feedAdmin)
