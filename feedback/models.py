from django.db import models

class feed(models.Model):
    text = models.TextField()
    on_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.on_date)
