from django.shortcuts import render
from feedback.models import feed
def myfeedback(request):
    if request.method=="POST":
        data = request.POST['message']
        obj = feed(text=data,)
        obj.save()
        return render(request,'feedback.html',{'status':'Thanks for your feedback'})

    return render(request,'feedback.html')